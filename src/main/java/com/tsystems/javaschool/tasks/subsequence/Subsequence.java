package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.isEmpty() && y.isEmpty())
            return true;
        else if (x.isEmpty())
            return true;
        int size = 0;
        for (Object elem : y) {
            if (size == x.size()) {
                return true;
            }
            if (Objects.equals(elem, x.get(size))) {
                size++;
            }
        }
        return x.size() == size;
    }
}
