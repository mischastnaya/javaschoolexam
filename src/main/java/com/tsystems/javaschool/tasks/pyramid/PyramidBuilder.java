package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size()> Integer.MAX_VALUE/2 || inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }
        List<Integer> numberList = new ArrayList<>(inputNumbers);
        Collections.sort(numberList);
        int countLines = getLineCountInPyramid(numberList.size());
        int lineSize = 1 + (countLines-1)*2;
        int pyramid[][] = new int[countLines][lineSize];
        Set<Integer> places = new HashSet<>();
        for (int i = 0; i < countLines; i++){
            Set<Integer> placesLoop = new HashSet<>(places);
            if(i==0){
                placesLoop.add(lineSize/2);
            }
            for (int m=0; m < lineSize; m++){
                pyramid[i][m] = 0;
                if(placesLoop.contains(m)){
                    pyramid[i][m] = numberList.get(0);
                    numberList.remove(0);
                    placesLoop.remove(m);
                    places.add(m-1);
                    places.add(m+1);
                    places.remove(m);
                }
            }
        }
        return pyramid;
    }

    private int getLineCountInPyramid(int arraySize) {
        double discriminant = 1 + 8*arraySize;
        double x1 = (-1 + Math.sqrt(discriminant)) / 2;
        double x2 = (-1 - Math.sqrt(discriminant)) / 2;
        if (x1 > 0 && x1%1 == 0){
            return (int) x1;
        }
        else if (x2 > 0 && x2%1 == 0){
            return (int) x2;
        }
        throw new CannotBuildPyramidException();
    }


}
